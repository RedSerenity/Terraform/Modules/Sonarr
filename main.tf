data "docker_registry_image" "Sonarr" {
	name = "linuxserver/sonarr:latest"
}

resource "docker_image" "Sonarr" {
	name = data.docker_registry_image.Sonarr.name
	pull_triggers = [data.docker_registry_image.Sonarr.sha256_digest]
}

module "Sonarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Sonarr.latest

	networks = [{ name: var.docker_network, aliases: ["sonarr.${var.internal_domain_base}"] }]
  ports = [{ internal: 8989, external: 9602, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Sonarr"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.tv}"
			container_path = "/tv"
			read_only = false
		},
		{
			host_path = "${var.downloads}"
			container_path = "/downloads"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}