variable "uid" {
	type = number
	default = 1000
}

variable "gid" {
	type = number
	default = 1000
}

variable "tz" {
	type = string
	default = "America/Denver"
}

variable "tv" {
	type = string
}

variable "downloads" {
	type = string
}

/* Docker Module Passthrough */
variable "name" {
	type = string
}

variable "docker_network" {
	type = string
}

variable "docker_data" {
	type = string
}

variable "stack" {
	type = string
}

variable "internal_domain_base" {
	type = string
	default = "flix"
}